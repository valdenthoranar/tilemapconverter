<?xml version="1.0" encoding="UTF-8"?>
<tileset name="test" tilewidth="8" tileheight="8">
 <image source="test.png" width="16" height="256"/>
 <tile id="4">
  <animation>
   <frame tileid="4" duration="500"/>
   <frame tileid="5" duration="500"/>
  </animation>
 </tile>
 <tile id="11">
  <animation>
   <frame tileid="11" duration="500"/>
   <frame tileid="12" duration="500"/>
  </animation>
 </tile>
</tileset>
