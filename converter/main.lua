SPRITE_INDEX = 3
COLLISION_INDEX = SPRITE_INDEX + 8
SPRITE_FILE = "test.png"

function love.load()
	output = assert(io.open("output/MHQ0000.DAT", "wb"),"Couldn't open the file. Check if the folder output exists")
	print(" == DATA ENCODER FOR MINI HERO QUEST ==")
	print("     created by Valden in lua using the LOVE library v0.9.1")

	-- Search all the ".lua" (maps) files
	dir = love.filesystem.getDirectoryItems("data")
	maps = {}
	for _,v in pairs(dir) do
		if string.find(v,".lua") then
			table.insert(maps,v)
		end
	end

	print("STEP 00 : CREATING DATA PARTITION")
	output:write("DAT")
	output:seek("set",0x10)
	print("\tdone")
	print("STEP 01 : ENCODING SPRITES")
	print("\tSearching for Tilesets ...")
	tilesetList = {};
	local maxTilesetId = 1
	for _,v in pairs(maps) do
		local f = love.filesystem.load("data/"..v)
		local map = f()
		print("\tprocessing " .. v)

		if tilesetList[map.tilesets.name] == nil then -- if a new tileset is found :
			print("\tnew tileset " .. map.tilesets[1].name .. " found")
			tilesetList[map.tilesets[1].name] = {}
			local curTileset = tilesetList[map.tilesets[1].name]
			curTileset.animation = {}
			curTileset.id = maxTilesetId
			maxTilesetId = maxTilesetId + 1

			curTileset.image = map.tilesets[1].image
			curTileset.collision = {}
			for i=1,32 do
				if map.tilesets[1].tiles[i] then
				if(map.tilesets[1].tiles[i].properties and map.tilesets[1].tiles[i].properties.collision) then
					curTileset.collision[i] = tonumber(map.tilesets[1].tiles[i].properties.collision)
				else
					curTileset.collision[i] = 0
				end
					--search if there is an animation data
					if map.tilesets[1].tiles[i].animation then
						local anim = {}
						anim.id = map.tilesets[1].tiles[i].id
						anim.length = table.getn(map.tilesets[1].tiles[i].animation)
						anim.frequence = math.floor(tonumber(map.tilesets[1].tiles[i].animation[1].duration)/50)
						table.insert(curTileset.animation,anim)
						print("\tFound one animation : id " .. anim.id .. ",length " .. anim.length .. ",freq " .. anim.frequence)
					end
				end
				
			end
		end
	end

	curPos = output:seek()
	print("\tProcessing sprites ...")
	for _, v in pairs(tilesetList) do
		print("\t\tProcessing " .. v.image)
		img = love.graphics.newImage("data/".. v.image)
		
		canvas = love.graphics.newCanvas(img:getWidth(),img:getHeight())
		love.graphics.setCanvas(canvas)
		love.graphics.draw(img,0,0)
		
		for k = 0,img:getHeight()-1, 8 do
			for i = 0,img:getWidth()-1 do
				if (k/8 * img:getWidth()/8 + i/8) >= 32 then break end
				local byte = 0;
				for j = 0,7 do
					local r,g,b,a = canvas:getPixel(i,j+k);
					if (r + g + b)/3 < 127 then
						byte = byte + 2^j
					end
				end
				--print(byte .. " ");
				output:write(string.char(byte))
			end
		end

		-- write the animation data
		indexPos = output:seek()
		for _,v in ipairs(v.animation) do
			output:write(string.char(v.id))
			output:write(string.char(v.length))
			output:write(string.char(v.frequence))
			output:write(string.char(0))
			print("\tWrote one animation")
		end

		output:seek("set", indexPos + 4*4) -- set the cursor so it's always at the end of the animation block

	end

	

	-- Write the position of the Sprite partition in the index
	nextSector = output:seek()
	output:seek("set",SPRITE_INDEX)
	writeBinary32(curPos)
	-- Write the position of the next sector (Maps) in the index
	writeBinary32(nextSector)
	output:seek("set",nextSector)
	print("done");

	print("STEP 02 : ENCODING MAPS")

	

	for _,v in pairs(maps) do
		local f = love.filesystem.load("data/"..v)
		local map = f()

		print("processing " .. v)



		if map.width > 255 or map.height > 255 then
			print("WARNING : file " .. v .. " is too big")
		end
		
		if not (map.tilesets[1].image == SPRITE_FILE) then
			print("WARNING : tileset " .. map.tilesets[1].image .. " has its spriteset different from the one used")
		end
		local currentMapAdrs = output:seek()
		
		output:seek("cur",4) --Leave some room for the next map adress
		output:write(string.char(map.width))
		output:write(string.char(map.height))
		output:write(string.char(tilesetList[map.tilesets[1].name].id))

		-- Write the map tile data
		for _,w in pairs(map.layers[1].data) do
  			output:write(string.char(w-1))
		end

		-- Write the next map adress
		local next_map = output:seek()
		output:seek("set",currentMapAdrs)
		writeBinary32(next_map);
		output:seek("set",next_map)
	end

	--[[
	-- Collision data :
	print("STEP 03 : ENCODING COLLISION DATA")
	curPos = output:seek()
	output:seek("set",COLLISION_INDEX)
	writeBinary32(curPos)
	output:seek("set",curPos)

	local i = 1;
	for i = 1, map.tilesets[1].imagewidth / 8 * map.tilesets[1].imageheight / 8 do
		v = map.tilesets[1].tiles[i];
		if(v.properties ~= nil and v.properties.collision == 1) then
			byte = byte + 2^((i%8)-1)
		end
		if i == 8 then
			output:write(string.char(byte))
		end
	end
	]]



	output:close()
	
	print("DATA ENCODING DONE");
	--love.event.quit();
end

function writeBinary32(input) 
	output:write(string.char((input / 2^24 )% 256))
	output:write(string.char((input / 2^16 )% 256))
	output:write(string.char((input / 2^8 )% 256))
	output:write(string.char(input  % 256))
end
